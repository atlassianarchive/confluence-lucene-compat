package com.atlassian.labs.lucenecompat;

import com.atlassian.bonnie.LuceneConnection;
import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.spring.container.ContainerManager;
import org.apache.lucene.document.Document;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Adapts {@link Extractor2} instances to {@link Extractor}.
 */
public class Extractor2ToExtractorAdapter implements Extractor
{
    private static final Logger log = LoggerFactory.getLogger(Extractor2ToExtractorAdapter.class);

    private ServiceTracker serviceTracker;

    private static final String adaptingExtractorClassName =
            "com.atlassian.labs.lucenecompat.impl." + (isLucene4() ? "Lucene4" : "Lucene29") + "AdaptingExtractor";

    private final Constructor<Extractor> adaptingExtractorConstructor;

    public Extractor2ToExtractorAdapter()
    {
        try
        {
            Class adaptingExtractorClass = this.getClass().getClassLoader().loadClass(adaptingExtractorClassName);
            adaptingExtractorConstructor = adaptingExtractorClass.getConstructor(Extractor2.class);
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void addFields(Document document, StringBuffer contentBody, Searchable searchable)
    {
        /**
         * Looks faster to fetch an array of service references than an array of services.
         */
        ServiceReference[] serviceReferences = getServiceTracker().getServiceReferences();

        if (serviceReferences == null)
            return;

        for (ServiceReference serviceReference : serviceReferences)
        {
            Object service = getServiceTracker().getService(serviceReference);

            if (!(service instanceof Extractor2))
                continue; // this shouldn't happen

            Extractor2 extractor2 = (Extractor2) service;

            try
            {
                adaptingExtractorConstructor.newInstance(extractor2).addFields(document, contentBody, searchable);
            }
            catch (RuntimeException e)
            {
                log.error("Error extracting search fields from " + searchable + " using " + extractor2  + ": " + e.getMessage(), e);
            }
            catch (InvocationTargetException e)
            {
                log.error("Error creating adapting extractor", e);
            }
            catch (InstantiationException e)
            {
                log.error("Error creating adapting extractor", e);
            }
            catch (IllegalAccessException e)
            {
                log.error("Error creating adapting extractor", e);
            }
        }
    }

    /**
     * This this a bit of hack. I had a really hard time getting hold of the OsgiContainerManager. I tried listening for
     * the OsgiContainerStartedEvent to no avail (I couldn'get my listener to pick up this event).
     *
     * More than happy for someone to clean this up. The track here is to expose a SeviceTracker over the Extractor2.class.
     *
     * Note: You cannot just inject a reference to OsgiContainerManager into this class. The bean isn't available for injecting on spring startup.
     */
    protected ServiceTracker getServiceTracker()
    {
        if (serviceTracker == null)
        {
            OsgiContainerManager osgiContainerManager = ((OsgiContainerManager) ContainerManager.getComponent("osgiContainerManager"));
            serviceTracker = osgiContainerManager.getServiceTracker(Extractor2.class.getName());
        }

        return serviceTracker;
    }

    private static boolean isLucene4()
    {
        try
        {
            Class.forName("org.apache.lucene.index.IndexableField", false, LuceneConnection.class.getClassLoader());
        }
        catch (ClassNotFoundException e)
        {
            return false;
        }

        return true;
    }

}
