This library helps a Confluence 5.2 targeted plugin run in previous version.
In 5.2 we've upgraded Lucene version to 4.2 and old Extractor plugin modules are not compatible anymore.

To use:
https://developer.atlassian.com/display/CONFDEV/How+to+fix+broken+Extractors

Issues:
https://jira.atlassian.com/browse/CONF (component "Searching/Indexing")