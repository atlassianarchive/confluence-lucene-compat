package com.atlassian.labs.lucenecompat.impl;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import org.apache.lucene.document.Field;

import java.util.HashMap;
import java.util.Map;

public class Lucene29FieldDescriptorMapper
{
    private static final Map<FieldDescriptor.Index, Field.Index> luceneIndexMapping = new HashMap<FieldDescriptor.Index, Field.Index>() {{
        put(FieldDescriptor.Index.NO, Field.Index.NO);
        put(FieldDescriptor.Index.NOT_ANALYZED, Field.Index.NOT_ANALYZED);
        put(FieldDescriptor.Index.ANALYZED, Field.Index.ANALYZED);
    }};

    private static final Map<FieldDescriptor.Store, Field.Store> luceneStoreMapping = new HashMap<FieldDescriptor.Store, Field.Store>() {{
        put(FieldDescriptor.Store.NO, Field.Store.NO);
        put(FieldDescriptor.Store.YES, Field.Store.YES);
    }};

    public Object map(FieldDescriptor fieldDescriptor)
    {
        Field.Store store = luceneStoreMapping.get(fieldDescriptor.getStore());
        Field.Index index = luceneIndexMapping.get(fieldDescriptor.getIndex());

        return new Field(fieldDescriptor.getName(), fieldDescriptor.getValue(), store, index);
    }
}
