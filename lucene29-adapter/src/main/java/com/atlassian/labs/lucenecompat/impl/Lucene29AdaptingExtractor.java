package com.atlassian.labs.lucenecompat.impl;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Fieldable;

import java.util.Collection;

/**
 * An extractor that is responsible for invoking instances of {@link Extractor2} in older versions of Confluence (pre 5.2).
 */
public class Lucene29AdaptingExtractor implements Extractor
{
    private final Extractor2 delegate;
    private final Lucene29FieldDescriptorMapper fieldDescriptorMapper;

    public Lucene29AdaptingExtractor(final Extractor2 delegate)
    {
        this.delegate = delegate;
        this.fieldDescriptorMapper = new Lucene29FieldDescriptorMapper();
    }

    public void addFields(final Document document, final StringBuffer defaultSearchableText, final Searchable searchable)
    {
        final Collection<FieldDescriptor> fieldDescriptors = delegate.extractFields(searchable);
        if (fieldDescriptors != null)
        {
            for (FieldDescriptor fieldDescriptor : fieldDescriptors)
            {
                Object field = fieldDescriptorMapper.map(fieldDescriptor);

                if (field instanceof Fieldable)
                    document.add((Fieldable) field);
            }
        }

        final StringBuilder buffer = delegate.extractText(searchable);
        if (buffer != null)
        {
            defaultSearchableText.append(' ').append(buffer);
        }
    }
}
