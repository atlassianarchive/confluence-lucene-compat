package com.atlassian.labs.lucenecompat.impl;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

import java.util.HashMap;
import java.util.Map;

public class Lucene4FieldDescriptorMapper
{
    private static final Map<FieldDescriptor.Index, Field.Index> luceneIndexMapping = new HashMap<FieldDescriptor.Index, Field.Index>() {{
        put(FieldDescriptor.Index.NO, Field.Index.NO);
        put(FieldDescriptor.Index.NOT_ANALYZED, Field.Index.NOT_ANALYZED);
        put(FieldDescriptor.Index.ANALYZED, Field.Index.ANALYZED);
    }};

    private static final Map<FieldDescriptor.Store, Field.Store> luceneStoreMapping = new HashMap<FieldDescriptor.Store, Field.Store>() {{
        put(FieldDescriptor.Store.NO, Field.Store.NO);
        put(FieldDescriptor.Store.YES, Field.Store.YES);
    }};

    public Object map(FieldDescriptor fieldDescriptor)
    {
        FieldDescriptor.Store store = fieldDescriptor.getStore();
        Field.Store fieldStore = luceneStoreMapping.get(store);

        FieldDescriptor.Index index = fieldDescriptor.getIndex();
        Field.Index fieldIndex = luceneIndexMapping.get(index);

        String fieldName = fieldDescriptor.getName();
        String fieldValue = fieldDescriptor.getValue();

        if (index == FieldDescriptor.Index.NOT_ANALYZED)
        {
            return new StringField(fieldName, fieldValue, fieldStore);
        }
        else if (index == FieldDescriptor.Index.ANALYZED)
        {
            return new TextField(fieldName, fieldValue, fieldStore);
        }
        else if (store == FieldDescriptor.Store.YES)
        {
            return new StoredField(fieldName, fieldValue);
        }

        // Not analyzed nor stored... pretty useless field
        return new Field(fieldName, fieldValue, fieldStore, fieldIndex);
    }
}
